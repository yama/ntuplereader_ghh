#include "histSvc.h"
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include <TKey.h>
#include <TList.h>
#include <TClass.h>
#include <TRegexp.h>
#include "TChain.h"
#include "TLorentzVector.h"
#include "generateHistogram.h"

#include <iostream>
#include <fstream>
#include <vector>
#include "TRandom3.h"

histSvc::histSvc( TString outputPath ):
  m_outputPath("./"),
  m_DSID(),
  m_flav_1(-1),
  m_flav_2(-1),
  m_njets(-1),
  m_nfjets(-1),
  m_ntags(-1),
  m_ptv(-1),
  m_flavor(""),
  m_description("")
{
  m_outputPath = outputPath;
  m_histograms.clear();
  m_histograms_2d.clear();
  m_histoMap.clear();
}

histSvc::~histSvc() {

}

void histSvc::SetAnalysisType( generateHistogram::AnalysisType type ) {

  m_type = type;

}

void histSvc::SetProcess( TString DSID, generateHistogram::Process process ){

  m_DSID = DSID;
  m_process = process;

}

void histSvc::SetEventFlav( int flav_1, int flav_2 ){

  m_flav_1 = flav_1;
  m_flav_2 = flav_2;

}

void histSvc::SetNjets( int njets ){

  m_njets = njets;

}
void histSvc::SetNFjets( int njets ){

   m_nfjets = njets;

}

void histSvc::SetpTV( float ptv ){

  m_ptv = ptv;

}

void histSvc::SetFlavor( int j1Flav,int j2Flav) {

  if       (j1Flav < 0) return;
  if       (j1Flav == 5 && j2Flav < 0) m_flavor = "b";
  else if  (j1Flav == 4 && j2Flav < 0) m_flavor = "c";
  else if  (j1Flav == 5 && j2Flav == 5)  m_flavor = "bb";
  else if ((j1Flav == 5 || j2Flav == 5)
        && (j1Flav == 4 || j2Flav == 4)) m_flavor = "bc";
  else if  (j1Flav == 5 || j2Flav == 5)  m_flavor = "bl";
  else if  (j1Flav == 4 && j2Flav == 4)  m_flavor = "cc";
  else if  (j1Flav == 4 || j2Flav == 4)  m_flavor = "cl";
  else m_flavor = "l";

}
void histSvc::SetNtags( int ntags ){

  m_ntags = ntags;

}


void histSvc::SetDescription( TString description){

  m_description = description;

}

void histSvc::BookFillHist( TString varName, bool isEl,bool isCBA,int nBins, float xmin, float xmax, float value, float weight) {

   TString histName = GetHistoName(varName,isEl,isCBA);
   auto t = m_histoMap.find(histName); 
    if (t == m_histoMap.end()){
	m_histoMap[histName] = m_histograms.size();
	m_histograms.push_back(new TH1F(histName,histName, nBins, xmin, xmax ));
    m_histograms.back()->GetXaxis()->SetTitle(varName);
	}
   m_histograms.at(m_histoMap[GetHistoName(varName,isEl,isCBA)])->Fill(value,weight);
}

void histSvc::BookFillHist( TString varName, bool isEl,bool isCBA,int nBinsX, float xmin, float xmax, float xvalue, int nBinsY, float ymin, float ymax, float yvalue, float weight) {
  
   TString histName = GetHistoName(varName, isEl,isCBA);
   auto t = m_histoMap.find(histName);
    if (t == m_histoMap.end()){
    m_histoMap[histName] = m_histograms_2d.size();
    m_histograms_2d.push_back(new TH2F(histName,histName, nBinsX, xmin, xmax, nBinsY, ymin, ymax));
    m_histograms_2d.back()->GetXaxis()->SetTitle(varName);
    }
   m_histograms_2d.at(m_histoMap[GetHistoName(varName, isEl,isCBA)])->Fill(xvalue,yvalue,weight);
}  





void histSvc::FillCutflow( int bin, float weight, TString name ){

  if(name == "Unweighted")  m_cutflow_Unweighted->Fill(bin,weight);
  else if(name == "EventWeight")  m_cutflow_EventWeight->Fill(bin,weight);
  else if(name == "FullWeight")  m_cutflow_FullWeight->Fill(bin,weight);

}

void histSvc::WriteHists() {

  TString outputFileName = m_outputPath;
  outputFileName += "/output_";
  if( m_type == generateHistogram::OneLepton ) outputFileName += "1Lep_";
  else if( m_type == generateHistogram::TwoLepton ) outputFileName += "2Lep_";
  else if( m_type == generateHistogram::ThreeLepton ) outputFileName += "3Lep_";
  outputFileName += m_DSID;
  outputFileName += ".root";
  TFile* f = new TFile(outputFileName,"RECREATE");
  TString histName;
  for( unsigned int i=0;i<m_histograms.size(); ++i ){ 
    histName = m_histograms.at(i)->GetName();
    m_histograms.at(i)->Write();//Yanhui
	delete m_histograms.at(i);
  }
  for( unsigned int ii=0;ii<m_histograms_2d.size(); ++ii ){
    histName = m_histograms_2d.at(ii)->GetName();
    m_histograms_2d.at(ii)->Write();//Yanhui
    delete m_histograms_2d.at(ii);
  }
  f->Close();  
  delete f;
}

TString histSvc::TruthFlavLabel(){

  // std::cout << "Inside TruthFlavLabel" << std::endl;
  // std::cout << m_flav_1 << " " << m_flav_2 << std::endl;
  // std::cout << m_flav_1+ m_flav_2 << std::endl;
  if(m_flav_1+ m_flav_2 == 10) return "bb";
  else if(m_flav_1+ m_flav_2 == 9) return "bc";
  else if(m_flav_1+ m_flav_2 == 5) return "bl";
  else if(m_flav_1+ m_flav_2 == 8) return "cc";
  else if(m_flav_1+ m_flav_2 == 4) return "cl";
  else if(m_flav_1+ m_flav_2 == 0) return "ll";
  else {
    std::cout << "Could not find truth flavour matching for " << m_flav_1+ m_flav_2 << " - exiting" << std::endl;
    exit(0);
  } 
}


TString histSvc::GetHistoName(TString varName, bool isEl, bool isCBA){

  TString histogramName = "";

  //if( m_type==generateHistogram::ZeroLepton ) histogramName += "0Lep";
  //else if( m_type==generateHistogram::OneLepton ) histogramName += "1Lep";
  //else if( m_type==generateHistogram::TwoLepton ) histogramName += "2Lep";

  //histogramName += "_";
  if( m_process==generateHistogram::nonprompt ){
      histogramName += "nonprompt";
  }
  
  if( m_process==generateHistogram::GHH300X ){
    histogramName += "GHH300X";
  }
  if( m_process==generateHistogram::GHH300Y ){
    histogramName += "GHH300Y";
  }
  if( m_process==generateHistogram::GHH600X ){
    histogramName += "GHH600X";
  }
  if( m_process==generateHistogram::GHH600Y ){
    histogramName += "GHH600Y";
  }
  if( m_process==generateHistogram::GHH900X ){
    histogramName += "GHH900X";
  }  
  if( m_process==generateHistogram::Wjets ){
    histogramName += "Wjets";//Yanhui
    //histogramName += m_flavor;
  }
  if( m_process==generateHistogram::Zjets ){
    histogramName += "Zjets";//Yanhui
    //histogramName += m_flavor;
  }
  if( m_process==generateHistogram::ttbar ){
    histogramName += "ttbar";
	//histogramName += m_flavor;//Yanhui temp
  }
  if( m_process==generateHistogram::stop ){
    histogramName += "stop";
  }
  if( m_process==generateHistogram::ssWW ){
    histogramName += "ssWW";
  }
  if( m_process==generateHistogram::WZ ){
    histogramName += "WZ";
  }
  if( m_process==generateHistogram::ZZ ){
    histogramName += "ZZ";
  }
  if( m_process==generateHistogram::ttW ){
    histogramName += "ttW";
  }
  if( m_process==generateHistogram::ttZ ){
    histogramName += "ttZ";
  }
  if( m_process==generateHistogram::tZ ){
    histogramName += "tZ";
  }
  if( m_process==generateHistogram::Wgamma ){
    histogramName += "Wgamma";
  }
  if( m_process==generateHistogram::Zgamma ){
    histogramName += "Zgamma";
  }
  if( m_process==generateHistogram::WWW ){
    histogramName += "WWW";
  }
  if( m_process==generateHistogram::VVV ){
    histogramName += "VVV";
  }
  if( m_process==generateHistogram::data ){
    histogramName += "data"; //dataCF
	}
  histogramName += "_";
  /*  
  if( m_ntags == 0 ) histogramName += "0tag";
  else if( m_ntags == 1 ) histogramName += "1tag";
  else if( m_ntags == 2 ) histogramName += "2tag";
  else if( m_ntags >= 3 ) histogramName += "3ptag";
  else if( m_ntags < 0 ) histogramName += "0ptag";
  
  if( m_njets == 1 ) histogramName += "1jet";
  else if( m_njets == 2 ) histogramName += "2jet";
  else if( m_njets == 3 ) histogramName += "3jet";
  else if( m_njets == 4 ) histogramName += "4jet";
  else if( m_njets >  4 ) histogramName += "5pjet";
  else if( m_njets < 0 ) histogramName += "0pjet";
  
  histogramName += "_";
  */
  /*
  if( m_nfjets == 0 ) histogramName += "0fjet";
  else if( m_nfjets == 1 ) histogramName += "1fjet";
  else if( m_nfjets == 2 ) histogramName += "2fjet";
  else if( m_nfjets == 3 ) histogramName += "3fjet";
  else if( m_nfjets > 3 ) histogramName += "4pfjet";
  else if( m_nfjets < 0 ) histogramName += "0pfjet";
  histogramName += "_";
  */
 /*
 if(!isCBA) {
  if( m_ptv > 0 && m_ptv < 75 ) histogramName += "0_75ptv";//Yanhui
  else if( m_ptv >= 75 &&m_ptv < 150) histogramName += "75_150ptv";//Yanhui
  else if( m_ptv >= 150 ) histogramName += "150ptv";
  else if( m_ptv < 0 ) histogramName += "0ptv";
 }
 else if (isCBA){
  if( m_ptv > 0 && m_ptv < 75 ) histogramName += "0_75ptv";//Yanhui
  else if( m_ptv >= 75 &&m_ptv < 150) histogramName += "75_150ptv";//Yanhui
  else if( m_ptv >= 150 &&m_ptv < 200) histogramName += "150_200ptv"; 
  else if( m_ptv >= 200) histogramName += "200ptv";
 }

  histogramName += "_";
  
  if(isCBA) m_description = "SR";
  */
  histogramName += m_description;
  histogramName += "_";

  histogramName += varName;
  //histogramName += "_";

  //histogramName += variation;


  histogramName.ReplaceAll(" ","");

  return histogramName;

}
