#include "generateHistogram.h"
#include "generateHistogram_2lep.h"
#include "ReadTruthTree.h"
#include <vector>
#include <iostream>

// This macro takes ntuples produced using the CxAODFramework framework as inputs (with a very loose event selection)
// The macro will then apply:
// * The event selection of your chosen analysis
// * fill the histograms
// * 

int main( int argc, char * argv[]  ){

  int sampleToRun = 0;
  if(argc > 1) sampleToRun = (std::atoi(argv[1]));
  TString outputPath = "./";
  if(argc > 2) outputPath = argv[2];
  const std::vector<TString> DSIDs = {"data15","data16","data17","data18","GHH600X","ttbar","singletop_t","singletop_s","singletop_Wt","Wjets","Zjets","WWW","VVV","Wgamma","Zgamma","ssWW","ttW","ttZ","tZ","WZ","ZZ","GHH300X","GHH300Y","GHH600Y","GHH900X"};
  TString NAME = DSIDs.at(sampleToRun);
  generateHistogram_2lep config; //2L;
  //generateHistogram_3lep config; // or run 3L;

  //config.SetInputPath("/unix/atlasvhbb2/yama/Ghh/Ntuples/ntuple-GHH-r02-01/submitDir-R-mc16ade/");
  //config.SetInputPath("/eos/user/y/yama/ntuple-GHH-r02-01/submitDir-R-mc16ade");
  config.SetInputPath("/unix/atlasvhbb2/yama/Ghh/Ntuples/ntuple-GHH-r02-01-20200921/MC16ade/ReduceSS2L");
  //or OS 3L samples  
  config.SetOutputPath(outputPath); //Set the output path for histograms
  config.SetNEvents(-1);  // Number of events to run over, -1 = all events
  config.SetDebug(false);  // Run in debug mode
  config.SetLumi(36.1);  // Set the luminosity to normalise MC to, in fb
  config.RunMJ(false);//Yanhui
  config.RunCBA(false);//Yanhui
  config.RunMC16a(false);//Yanhui
  config.UseSFs(false);//Yanhui
  config.TightIso(false);//Yanhui
  //Do you aply the tau veto?!!!
  config.GenerateHistogram(generateHistogram::TwoLepton,DSIDs.at(sampleToRun)); //2L
  //config.GenerateHistogram(generateHistogram::ThreeLepton,DSIDs.at(sampleToRun)); //or run 3L
}

