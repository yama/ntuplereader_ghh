#include "MVATree.h"
#include "ReadTruthTree.h"
#include "generateHistogram.h"
#include <TFile.h>
#include <TH1.h>
#include <TKey.h>
#include <TList.h>
#include <TClass.h>
#include <TRegexp.h>
#include "TChain.h"
#include "TLorentzVector.h"

#include <iostream>
#include <fstream>
#include <vector>
#include "TRandom3.h"

#include "TMVA/Config.h"
#include "TMVA/Factory.h"
#include "TMVA/Tools.h"
#include "TMVA/Reader.h"

MVATree::MVATree(generateHistogram::AnalysisType type):
  reader_2jet_even(NULL),
  reader_2jet_odd(NULL),
  reader_3jet_even(NULL),
  reader_3jet_odd(NULL)
{

  std::cout << "Initialising TMVA" << std::endl;

  if( type == generateHistogram::OneLepton){
    TString FilePath_1L_2J_even = "./data/TMVAClassification_1lep2jet_0of2_Sherpa221_5node_BDT_AdaBoost_v9_200Tree_4Depth_5node.weights.xml";
    TString FilePath_1L_2J_odd = "./data/TMVAClassification_1lep2jet_1of2_Sherpa221_5node_BDT_AdaBoost_v9_200Tree_4Depth_5node.weights.xml";
    TString FilePath_1L_3J_even = "./data/TMVAClassification_1lep3jet_0of2_Sherpa221_5node_BDT_AdaBoost_v9_200Tree_4Depth_5node.weights.xml";
    TString FilePath_1L_3J_odd = "./data/TMVAClassification_1lep3jet_1of2_Sherpa221_5node_BDT_AdaBoost_v9_200Tree_4Depth_5node.weights.xml";

    reader_2jet_even = InitReader(type, generateHistogram::TwoJetSR, "BDT_1L_2jet_even",FilePath_1L_2J_even);
    reader_2jet_odd = InitReader(type, generateHistogram::TwoJetSR, "BDT_1L_2jet_odd",FilePath_1L_2J_odd);
    reader_3jet_even = InitReader(type, generateHistogram::ThreeJetSR, "BDT_1L_3jet_even",FilePath_1L_3J_even);
    reader_3jet_odd = InitReader(type, generateHistogram::ThreeJetSR, "BDT_1L_3jet_odd",FilePath_1L_3J_odd);

  }

}

MVATree::~MVATree() {

  delete reader_2jet_even;
  delete reader_2jet_odd;
  delete reader_3jet_even;
  delete reader_3jet_odd;

}


TMVA::Reader* MVATree::InitReader(const generateHistogram::AnalysisType type, const generateHistogram::AnalysisRegion reg, const TString Name, const TString WeightFile){


  TMVA::Reader* reader = new TMVA::Reader( "!Color:!Silent" );

  if( type == generateHistogram::OneLepton ){

      reader->AddVariable("dRBB", &dRBB);
      reader->AddVariable("mBB", &mBB);
      reader->AddVariable("dPhiVBB", &dPhiVBB);
      reader->AddVariable("dPhiLBmin", &dPhiLBmin);
      reader->AddVariable("pTV", &pTV);
      reader->AddVariable("pTB1", &pTB1);
      reader->AddVariable("pTB2", &pTB2);
      reader->AddVariable("mTW", &mTW);
      reader->AddVariable("Mtop", &Mtop);
      reader->AddVariable("dYWH", &dYWH);
      reader->AddVariable("MET", &MET);
      
    if( reg == generateHistogram::ThreeJetSR ){
      reader->AddVariable("mBBJ", &mBBJ);
      reader->AddVariable("pTJ3", &pTJ3);
    }
  }
  reader->BookMVA(Name,WeightFile);
  return reader;

}
