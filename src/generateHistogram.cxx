#include "generateHistogram.h"
#include "ReadTruthTree.h"
#include "MVATree.h"  
#include "histSvc.h"

#include <TFile.h>
#include <TH1.h>
#include <TKey.h>
#include <TList.h>
#include <TClass.h>
#include <TRegexp.h>
#include "TChain.h"
#include "TLorentzVector.h"

#include <iostream>
#include <fstream>
#include <vector>
#include "TRandom3.h"
#include <dirent.h>

generateHistogram::generateHistogram():
  m_nEvents(-1),
  m_DSID(""),
  m_Debug(false),
  m_Lumi(1000),
  m_SumWeights(0),
  m_XSec(0),
  //Yanhui
  m_MJ(false),
  m_CBA(false),
  m_MC16a(false),
  m_UseSFs(false),
  m_TightIso(false),
  m_inputChain(NULL),
  m_outputPath("./"),
  m_inputPath("./"),
  hTruthMap_b(NULL),
  hTruthMap_c(NULL),
  hTruthMap_l(NULL),
  hTruthMap_t(NULL)
{

}

generateHistogram::~generateHistogram() {

}

void generateHistogram::SetInputPath(TString path){

  m_inputPath = path;

}

TString generateHistogram::GetOutputPath(){

  return m_outputPath;

}

void generateHistogram::SetOutputPath(TString path){

  m_outputPath = path;

  std::cout << "m_outputPath " << m_outputPath << std::endl;

}

void generateHistogram::SetNEvents(int events){

  m_nEvents = events;

}

void generateHistogram::SetDebug(bool debug){

  m_Debug = debug;

}

void generateHistogram::SetLumi(float lumi){

  m_Lumi = lumi * 1000;  //Convert fb-1 to pb-1
}

void generateHistogram::RunMJ(bool MJ){

   m_MJ = MJ;
}
void generateHistogram::RunCBA(bool CBA){

   m_CBA = CBA;
}
void generateHistogram::RunMC16a(bool MC16a){

   m_MC16a = MC16a;
}
void generateHistogram::UseSFs(bool UseSFs){

   m_UseSFs = UseSFs;
}
void generateHistogram::TightIso(bool TightIso){

  m_TightIso = TightIso;
}
void generateHistogram::GenerateHistogram( AnalysisType type, TString DSID){
//void generateHistogram::GenerateHistogram( AnalysisType type, Process process, Variation variation){

  StoreConfig(type, DSID);
  FileLoader(DSID);
  std::cout << m_inputChain->GetEntries() << std::endl;
  //m_tree = new MVATree(type);//Yanhui
  //std::cout<<"here1"<<"\n";
  m_histFill = new histSvc(m_outputPath);
  //std::cout<<"here2"<<"\n";
  m_histFill->SetAnalysisType(type);
  std::cout<<"here3"<<"\n";
  m_histFill->SetProcess(DSID, m_process);
  //std::cout<<"here4"<<"\n";
  readNtuple = new ReadTruthTree(m_inputChain);
  //std::cout<<"here5"<<"\n";

  FillHistograms(DSID);//Here is the loop
  //std::cout<<"here6"<<"\n";
  m_histFill->WriteHists();

  delete m_tree;
  delete m_histFill;
  delete readNtuple;
  delete m_inputChain;

}


void generateHistogram::StoreConfig( AnalysisType type, TString DSID ){

  m_type = type;
  m_DSID = DSID;
  m_process = GetProcessFromDSID(DSID);
//  m_variation = variation;
//  std::cout << m_region.size() << std::endl;
//  std::cout << m_variable.size() << std::endl;


}

generateHistogram::Process generateHistogram::GetProcessFromDSID( TString DSID ){

  std::cout << "DSID " << DSID << std::endl;
  if(DSID.Contains("GHH300X")) return generateHistogram::GHH300X;
  if(DSID.Contains("GHH300Y")) return generateHistogram::GHH300Y;
  if(DSID.Contains("GHH600X")) return generateHistogram::GHH600X;
  if(DSID.Contains("GHH600Y")) return generateHistogram::GHH600Y;
  if(DSID.Contains("GHH900X")) return generateHistogram::GHH900X;
  if( DSID.Contains("Wjets") ) return generateHistogram::Wjets;
  if( DSID.Contains("Zjets")) return generateHistogram::Zjets;
  if( DSID.Contains("ttbar") ) return generateHistogram::ttbar;
  if( DSID.Contains("singletop_s") || DSID.Contains("singletop_t") || DSID.Contains("singletop_Wt") ) return generateHistogram::stop;
  if( DSID.Contains("ssWW")) return generateHistogram::ssWW;
  if( DSID.Contains("WZ")) return generateHistogram::WZ;
  if( DSID.Contains("ZZ")) return generateHistogram::ZZ;
  if( DSID.Contains("Wgamma")) return generateHistogram::Wgamma;
  if( DSID.Contains("Zgamma")) return generateHistogram::Zgamma;
  if( DSID.Contains("WWW")) return generateHistogram::WWW;
  if( DSID.Contains("VVV")) return generateHistogram::VVV;
  if( DSID.Contains("ttW")) return generateHistogram::ttW;
  if( DSID.Contains("ttZ")) return generateHistogram::ttZ;
  if( DSID.Contains("tZ")) return generateHistogram::tZ;
  if( DSID.Contains("data")) return generateHistogram::data;
  else {
    std::cout << "Did not find sample - exiting" << std::endl;
    exit(0);
  }
}


void generateHistogram::FileLoader(TString DSID ){
  
//TODO Add file loading based on process (i.e. if Whf, load all W+jets samples)
  
  TString ChainName = "Nominal";
  //ChainName += DSID;

  m_inputChain = new TChain(ChainName);

  TH1F* h_SumW;
  TH1F* h_XSec;

  TString SumWName = "h_Keep_EventCount_";
  //SumWName += DSID;
  TString XSecName = "h_Keep_CrossSection_";
  //XSecName += DSID;

  TString DSIDStr = "";
  DSIDStr += DSID;
  m_SumWeights = 0;
  m_XSec = 0;

  if( m_inputPath.Contains(".root") ){
    m_inputChain->Add(m_inputPath);
    TFile* loadFile = TFile::Open(m_inputPath);
    h_SumW = (TH1F*) loadFile->Get(SumWName);
    h_XSec = (TH1F*) loadFile->Get(XSecName);
    m_SumWeights = h_SumW->GetBinContent(1);
    m_XSec = h_XSec->GetBinContent(1) / h_XSec->GetEntries();  //Need to divide by entries due to grid merging
  }
  else {
    std::cout << "Input is a directory: going fancy " << std::endl;
	///eos/atlas/user/y/yama/HtRatio/Ntuples/Reduce /*root
	DIR*     dir;
    dirent*  pdir;
    dir = opendir( m_inputPath );     // open current directory
	while ((pdir = readdir(dir)))  {
	  TString foldName=pdir->d_name;//data16-new.root
	  //std::cout<<"foldName == "<<foldName<<"\n";
      if(!foldName.Contains( DSIDStr )) continue;
	  std::cout <<"input files is "<< pdir->d_name << std::endl;
      DIR*     dir2;
      dirent*  pdir2;
      dir2 = opendir( (m_inputPath+"/"+foldName) );     // open current directory
      if (!foldName.Contains("root")) continue;//Yanhui
	  m_inputChain->Add( ( m_inputPath+"/"+foldName) );

	  /*
	  while ((pdir2 = readdir(dir2)))  {
	TString fName=pdir2->d_name;
	//cout << " fName: " << fName << endl;
	if (!fName.Contains("root")) continue;//Yanhui
	TString temp = m_inputPath+"/"+foldName+"/"+fName;
	m_inputChain->Add( ( m_inputPath+"/"+foldName+"/"+fName) );
	if(m_Debug) std::cout << fName << std::endl;
	TFile* loadFile = TFile::Open( m_inputPath+"/"+foldName+"/"+fName );
	h_SumW = (TH1F*) loadFile->Get(SumWName);
	h_XSec = (TH1F*) loadFile->Get(XSecName);
	m_SumWeights += h_SumW->GetBinContent(1);
	if( m_XSec == 0 )     m_XSec = h_XSec->GetBinContent(1) / h_XSec->GetEntries();  //Need to divide by entries due to grid merging
      }
	  */

    }
  } 
  //std::cout << "m_SumWeights " << m_SumWeights << std::endl;
  //std::cout << "m_XSec " << m_XSec << std::endl;
      
}


void generateHistogram::FillHistograms(TString DSID){
//Dummy function to be overwritten
}


float generateHistogram::getTagEff(TLorentzVector jet, int jetFlav){

  if( jetFlav == 5 ) {
    return hTruthMap_b->GetBinContent(hTruthMap_b->GetXaxis()->FindBin(fabs(jet.Eta())), hTruthMap_b->GetYaxis()->FindBin(jet.Pt()));
  }
  else if ( jetFlav == 4 ) {
    return hTruthMap_c->GetBinContent(hTruthMap_c->GetXaxis()->FindBin(fabs(jet.Eta())), hTruthMap_c->GetYaxis()->FindBin(jet.Pt()));
  }
  else if ( jetFlav == 15 ) {
    return hTruthMap_t->GetBinContent(hTruthMap_t->GetXaxis()->FindBin(fabs(jet.Eta())), hTruthMap_t->GetYaxis()->FindBin(jet.Pt()));
  }
  else if ( jetFlav == 0 ) {
    return hTruthMap_l->GetBinContent(hTruthMap_l->GetXaxis()->FindBin(fabs(jet.Eta())), hTruthMap_l->GetYaxis()->FindBin(jet.Pt()));
  }
  else if ( jetFlav == -999 ) return 0.0;
  else{
    std::cout << "Jet flavour not found - exiting" << std::endl;
    exit(0);
  }
}

int generateHistogram::roll_dice(const std::vector<float>&choices){
  static TRandom3 rand;
  double total=0;
  rand.SetSeed(123456);
//  rand.SetSeed(time(NULL));
  for(auto choice:choices)total+=choice;
  double dice=rand.Rndm(),choice=total*dice,tat=0;
//   cout<<"total: "<<total<<", choice: "<<choice<<", dice: "<<dice<<endl;
  for(unsigned int i=0;i<choices.size();i++){
    tat+=choices.at(i);
    if(choice<tat)return i;
  }
  return choices.size()-1;
}

