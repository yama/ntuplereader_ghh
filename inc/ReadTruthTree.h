//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed Oct 25 16:20:48 2017 by ROOT version 5.34/36
// from TTree ReadTruthTree/
// found on file: ntuple.root
//////////////////////////////////////////////////////////

#ifndef ReadTruthTree_h
#define ReadTruthTree_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include <vector>
#include <string.h>
#include <iostream>
#include <fstream>
using namespace std;
// Fixed size dimensions of array or collections stored in the TTree if any.

class ReadTruthTree {
//using namespace std;
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

   // Declaration of leaf types
   float weight;
   unsigned long long runNumber;
   unsigned long long eventNumber;
   bool passGHHSS2lSR; //2L merged or resolved
   bool passSS2lSRreg1; //2L merged  passTrig && passTrigMatch && !pass3Lepton && passSameSign
   bool passSS2lSRreg2; //2L resolved
   bool passTrig;
   bool passTrigMatch;
   int Nleptons;
   int Nfatjets;
   bool passSameSign;
   bool pass3Lepton;
   float fatjet1Pt; //200-500 medium; >500 boosted
   float met;
   float metSig;
   float metSigRatio;
   float Mll;
   float Lep1Pt;
   float Lep2Pt;
   float Lep3Pt;
   float Reg3lPtL3;
   float Jet1Pt;
   float Jet1Eta;
   float Jet2Eta;
   float Jet2Pt;
   int Nbjets;
   int Nbjets77;
   bool Lep1isTight;
   bool Lep2isTight;
   bool  Lep1isLoose;
   bool  Lep1isNoBL;
   bool  Lep2isLoose;
   bool  Lep2isNoBL;
   int Lep1Flav;
   int Lep2Flav;
   float mV3;
   float dPhill;
   float PtV3;
   float Mjj;
   float Ptjj;
   float fatjet1M;

   bool pass4Lepton;
   int  Lep3Flav;
   bool Lep3isLoose;
   bool Lep3isNoBL;
   bool passLeptonPt;
   float Mlll;
   int Lep1Q;
   int Lep2Q;
   bool pass3LRegion;//NLeptons==3 and one SFOS leptons
   float Reg3lMll;//mZ
   float Reg3lMhh;//mH
   float Reg3lPthh;//pTH
   int   region3l; // 1,2,3,4
   int   Njets;
   int   regionSS2l;

   float DRlllv;
   float DRlljj;
   float DRllJ;
   float DRlvjj;
   float DRlvJ;
   float mHHllJ;
   float ptHHllJ;
   float mHHlvJ;
   float ptHHlvJ;
   float mHHlljj;
   float ptHHlljj;
   float mHHlvjj;
   float ptHHlvjj;
   float mWlv;
   float pTWlv;
   float mZll;
   float pTZll;
   bool  Lep3isTight;
   bool Lep1isSignalL;
   bool Lep1isLooseL;
   bool Lep1isAntiBLL;
   bool Lep1isPLVTightL;
   bool Lep1isECIDSL;
   bool Lep2isSignalL;
   bool Lep2isLooseL;
   bool Lep2isAntiBLL;
   bool Lep2isPLVTightL;
   bool Lep2isECIDSL;
   bool Lep3isSignalL;
   bool Lep3isLooseL;
   bool Lep3isAntiBLL;
   bool Lep3isPLVTightL;
   bool Lep3isECIDSL;
   bool fj1_isWJet50;
   bool fj1_isWJet80;
   bool fj1_isZJet50;
   bool fj1_isZJet80;
   float Lep1Eta;
   float Lep2Eta;
   float Lep1d0;
   float Lep2d0;
   float Lep3d0;
   int Lep1truthStatus;
   int Lep2truthStatus;
   int Lep3truthStatus;
   int Nel;
   float mTWL1;
   float mTWL2;
   float mTW;
   float dPhiL1FJ1;
   float dPhiL2FJ1;
   float dEtaL1FJ1;
   float dEtaL2FJ1;
   float dRL1FJ1;
   float dRL2FJ1;
   float dPhiJ1J2;
   float dEtaJ1J2;
   float dRJ1J2;
   float dPhiL1J1;
   float dPhiL2J1;
   float dEtaL1J1;
   float dEtaL2J1;
   float dRL1J1;
   float dRL2J1;
   float dPhiL1J2;
   float dPhiL2J2;
   float dEtaL1J2;
   float dEtaL2J2;
   float dRL1J2;
   float dRL2J2;
   float dPhiL1W;
   float dPhiL2W;
   float dEtaL1W;
   float dEtaL2W;
   float dRL1W;
   float dRL2W;
   float dEtall;
   int Lep1Author;
   int Lep1AddAmbiguity;
   int Lep2Author;
   int Lep2AddAmbiguity;
   int Lep3Author;
   int Lep3AddAmbiguity;
   TBranch        *b_Reg3lPtL3;   //!
   TBranch        *b_Lep1Author;   //!
   TBranch        *b_Lep1AddAmbiguity;   //!
   TBranch        *b_Lep2Author;   //!
   TBranch        *b_Lep2AddAmbiguity;   //!
   TBranch        *b_Lep3Author;   //!
   TBranch        *b_Lep3AddAmbiguity;   //!
   TBranch        *b_dEtall;   //!
   TBranch        *b_mTWL1;   //!
   TBranch        *b_mTWL2;   //!
   TBranch        *b_mTW;   //!
   TBranch        *b_dPhiL1FJ1;   //!
   TBranch        *b_dPhiL2FJ1;   //!
   TBranch        *b_dEtaL1FJ1;   //!
   TBranch        *b_dEtaL2FJ1;   //!
   TBranch        *b_dRL1FJ1;   //!
   TBranch        *b_dRL2FJ1;   //!
   TBranch        *b_dPhiJ1J2;   //!
   TBranch        *b_dEtaJ1J2;   //!
   TBranch        *b_dRJ1J2;   //!
   TBranch        *b_dPhiL1J1;   //!
   TBranch        *b_dPhiL2J1;   //!
   TBranch        *b_dEtaL1J1;   //!
   TBranch        *b_dEtaL2J1;   //!
   TBranch        *b_dRL1J1;   //!
   TBranch        *b_dRL2J1;   //!
   TBranch        *b_dPhiL1J2;   //!
   TBranch        *b_dPhiL2J2;   //!
   TBranch        *b_dEtaL1J2;   //!
   TBranch        *b_dEtaL2J2;   //!
   TBranch        *b_dRL1J2;   //!
   TBranch        *b_dRL2J2;   //!
   TBranch        *b_dPhiL1W;   //!
   TBranch        *b_dPhiL2W;   //!
   TBranch        *b_dEtaL1W;   //!
   TBranch        *b_dEtaL2W;   //!
   TBranch        *b_dRL1W;   //!
   TBranch        *b_dRL2W;   //!
   TBranch        *b_Nel;   //!
   TBranch        *b_Lep3d0;   //!
   TBranch        *b_Lep3truthStatus;   //!
   TBranch        *b_Lep1truthStatus;   //!
   TBranch        *b_Lep2truthStatus;   //!
   TBranch        *b_Lep1d0;   //!
   TBranch        *b_Lep2d0;   //!
   TBranch        *b_Lep1Eta;   //!
   TBranch        *b_Lep2Eta;   //!
   TBranch        *b_DRlllv;   //!
   TBranch        *b_DRlljj;   //!
   TBranch        *b_DRllJ;   //!
   TBranch        *b_DRlvjj;   //!
   TBranch        *b_DRlvJ;   //!
   TBranch        *b_mHHllJ;   //!
   TBranch        *b_ptHHllJ;   //!
   TBranch        *b_mHHlvJ;   //!
   TBranch        *b_ptHHlvJ;   //!
   TBranch        *b_mHHlljj;   //!
   TBranch        *b_ptHHlljj;   //!
   TBranch        *b_mHHlvjj;   //!
   TBranch        *b_ptHHlvjj;   //!
   TBranch        *b_mWlv;   //!
   TBranch        *b_pTWlv;   //!
   TBranch        *b_mZll;   //!
   TBranch        *b_pTZll;   //!
   TBranch        *b_Lep3isTight;   //!
   TBranch        *b_Lep1isSignalL;   //!
   TBranch        *b_Lep1isLooseL;   //!
   TBranch        *b_Lep1isAntiBLL;   //!
   TBranch        *b_Lep1isPLVTightL;   //!
   TBranch        *b_Lep1isECIDSL;   //!
   TBranch        *b_Lep2isSignalL;   //!
   TBranch        *b_Lep2isLooseL;   //!
   TBranch        *b_Lep2isAntiBLL;   //!
   TBranch        *b_Lep2isPLVTightL;   //!
   TBranch        *b_Lep2isECIDSL;   //!
   TBranch        *b_Lep3isSignalL;   //!
   TBranch        *b_Lep3isLooseL;   //!
   TBranch        *b_Lep3isAntiBLL;   //!
   TBranch        *b_Lep3isPLVTightL;   //!
   TBranch        *b_Lep3isECIDSL;   //! 
   TBranch        *b_fj1_isWJet50;   //!
   TBranch        *b_fj1_isWJet80;   //!
   TBranch        *b_fj1_isZJet50;   //!
   TBranch        *b_fj1_isZJet80;   //!
   TBranch        *b_regionSS2l;   //!
   TBranch        *b_Njets;   //!
   TBranch        *b_pass3LRegion;   //!
   TBranch        *b_Reg3lMll;   //!
   TBranch        *b_Reg3lMhh;   //!
   TBranch        *b_Reg3lPthh;   //!
   TBranch        *b_region3l;   //!
   TBranch        *b_pass4Lepton;   //!
   TBranch        *b_Lep3Flav;   //!
   TBranch        *b_Lep3isLoose;   //!
   TBranch        *b_Lep3isNoBL;   //!
   TBranch        *b_passLeptonPt;   //!
   TBranch        *b_Mlll;   //!
   TBranch        *b_Lep1Q;   //!
   TBranch        *b_Lep2Q;   //!
   
   TBranch        *b_Mjj;   //!
   TBranch        *b_Ptjj;   //!
   TBranch        *b_fatjet1M;   //!
   TBranch        *b_passTrig;   //!
   TBranch        *b_passTrigMatch;   //!
   TBranch        *b_Nleptons;   //!
   TBranch        *b_Nfatjets;   //!
   TBranch        *b_passSameSign;   //!
   TBranch        *b_pass3Lepton;   //!
   TBranch        *b_PtV3;   //!
   TBranch        *b_dPhill;   //!
   TBranch        *b_Lep1Flav;   //!
   TBranch        *b_Lep2Flav;   //!
   TBranch        *b_mV3;   //!
   TBranch        *b_weight;   //!
   TBranch        *b_runNumber;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_passGHHSS2lSR;   //!
   TBranch        *b_passSS2lSRreg1;   //!
   TBranch        *b_passSS2lSRreg2;   //!
   TBranch        *b_fatjet1Pt;   //!
   TBranch        *b_met;   //!
   TBranch        *b_metSig;   //!
   TBranch        *b_metSigRatio;   //!
   TBranch        *b_Mll;   //!
   TBranch        *b_Lep1Pt;   //!
   TBranch        *b_Lep2Pt;   //!
   TBranch        *b_Lep3Pt;   //!
   TBranch        *b_Jet1Pt;   //!
   TBranch        *b_Jet2Pt;   //!
   TBranch        *b_Jet1Eta;   //!
   TBranch        *b_Jet2Eta;   //!
   TBranch        *b_Nbjets;   //!
   TBranch        *b_Nbjets77;   //!
   TBranch        *b_Lep1isTight;   //!
   TBranch        *b_Lep2isTight;   //!
   TBranch        *b_Lep1isLoose;   //!
   TBranch        *b_Lep1isNoBL;   //!
   TBranch        *b_Lep2isLoose;   //!
   TBranch        *b_Lep2isNoBL;   //!

   TBranch        *b_lmetdPhi;   //!
   TBranch        *b_jmetdPhi;   //!
   TBranch        *b_jldPhi;   //!
   //TBranch        *b_jjmindPhi;   //!
   


   ReadTruthTree(TTree *tree=0);
   virtual ~ReadTruthTree();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef ReadTruthTree_cxx
ReadTruthTree::ReadTruthTree(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
/*
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("ntuple.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("ntuple.root");
      }
      f->GetObject("ReadTruthTree",tree);

   }
*/
//std::cout<<"WTF?"<<"\n";
Init(tree);

}

ReadTruthTree::~ReadTruthTree()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t ReadTruthTree::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t ReadTruthTree::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void ReadTruthTree::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   Reg3lPtL3=-99;
   weight=-99;
   runNumber=-99;
   eventNumber=-99;
   passGHHSS2lSR=-99;
   passSS2lSRreg1=-99;
   passSS2lSRreg2=-99;
   fatjet1Pt=-99;
   met=-99;
   metSig=-99;
   metSigRatio=-99;
   Mll=-99;
   Lep1Pt=-99;
   Lep2Pt=-99;
   Lep3Pt=-99;
   Jet1Pt=-99;
   Jet2Pt=-99;
   Jet1Eta=-99;
   Jet2Eta=-99;
   Nbjets=-99;
   Nbjets77=-99;
   Lep1isTight=-99;
   Lep2isTight=-99;
   Lep1isLoose=-99;
   Lep1isNoBL=-99;
   Lep2isLoose=-99;
   Lep2isNoBL=-99;
   Lep1Flav=-99;
   Lep2Flav=-99;
   mV3=-99;
   dPhill=-99;
   PtV3=-99;
   passTrig=-99;
   passTrigMatch=-99;
   Nleptons=-99;
   passSameSign=-99;
   pass3Lepton=-99;
   Nfatjets=-99;
   Mjj=-99;
   Ptjj=-99;
   fatjet1M=-99;

   pass4Lepton=-99;
   Lep3Flav=-99;
   Lep3isLoose=-99;
   Lep3isNoBL=-99;
   passLeptonPt=-99;
   Mlll=-99;
   Lep1Q=-99;
   Lep2Q=-99;
   pass3LRegion=-99;
   Reg3lMll=-99;
   Reg3lMhh=-99;
   Reg3lPthh=-99;
   region3l=-99;
   regionSS2l=-99;
   Njets=-99;
   DRlllv=-99;
   DRlljj=-99;
   DRllJ=-99;
   DRlvjj=-99;
   DRlvJ=-99;
   mHHllJ=-99;
   ptHHllJ=-99;
   mHHlvJ=-99;
   ptHHlvJ=-99;
   mHHlljj=-99;
   ptHHlljj=-99;
   mHHlvjj=-99;
   ptHHlvjj=-99;
   mWlv=-99;
   pTWlv=-99;
   mZll=-99;
   pTZll=-99;
   Lep3isTight=-99;
   Lep1isSignalL=-99;
   Lep1isLooseL=-99;
   Lep1isAntiBLL=-99;
   Lep1isPLVTightL=-99;
   Lep1isECIDSL=-99;
   Lep2isSignalL=-99;
   Lep2isLooseL=-99;
   Lep2isAntiBLL=-99;
   Lep2isPLVTightL=-99;
   Lep2isECIDSL=-99;
   Lep3isSignalL=-99;
   Lep3isLooseL=-99;
   Lep3isAntiBLL=-99;
   Lep3isPLVTightL=-99;
   Lep3isECIDSL=-99;
   fj1_isWJet50=-99;
   fj1_isWJet80=-99;
   fj1_isZJet50=-99;
   fj1_isZJet80=-99; 
   Lep1Eta=-99;
   Lep2Eta=-99;
   Lep1d0=-99;
   Lep2d0=-99;
   Lep3d0=-99;
   Lep1truthStatus=-99;
   Lep2truthStatus=-99;
   Lep3truthStatus=-99;
   Nel=-99;
   mTWL1=-99;
   mTWL2=-99;
   mTW=-99;
   dPhiL1FJ1=-99;
   dPhiL2FJ1=-99;
   dEtaL1FJ1=-99;
   dEtaL2FJ1=-99;
   dRL1FJ1=-99;
   dRL2FJ1=-99;
   dPhiJ1J2=-99;
   dEtaJ1J2=-99;
   dRJ1J2=-99;
   dPhiL1J1=-99;
   dPhiL2J1=-99;
   dEtaL1J1=-99;
   dEtaL2J1=-99;
   dRL1J1=-99;
   dRL2J1=-99;
   dPhiL1J2=-99;
   dPhiL2J2=-99;
   dEtaL1J2=-99;
   dEtaL2J2=-99;
   dRL1J2=-99;
   dRL2J2=-99;
   dPhiL1W=-99;
   dPhiL2W=-99;
   dEtaL1W=-99;
   dEtaL2W=-99;
   dRL1W=-99;
   dRL2W=-99;
   dEtall=-99;
   Lep1Author=-99;
   Lep1AddAmbiguity=-99;
   Lep2Author=-99;
   Lep2AddAmbiguity=-99;
   Lep3Author=-99;
   Lep3AddAmbiguity=-99;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);
   fChain->SetBranchAddress("Reg3lPtL3", &Reg3lPtL3, &b_Reg3lPtL3);
   fChain->SetBranchAddress("Lep1Author", &Lep1Author, &b_Lep1Author);
   fChain->SetBranchAddress("Lep1AddAmbiguity", &Lep1AddAmbiguity, &b_Lep1AddAmbiguity);
   fChain->SetBranchAddress("Lep2Author", &Lep2Author, &b_Lep2Author);
   fChain->SetBranchAddress("Lep2AddAmbiguity", &Lep2AddAmbiguity, &b_Lep2AddAmbiguity);
   fChain->SetBranchAddress("Lep3Author", &Lep3Author, &b_Lep3Author);
   fChain->SetBranchAddress("Lep3AddAmbiguity", &Lep3AddAmbiguity, &b_Lep3AddAmbiguity);
   fChain->SetBranchAddress("dEtall", &dEtall, &b_dEtall);
   fChain->SetBranchAddress("mTWL1", &mTWL1, &b_mTWL1);
   fChain->SetBranchAddress("mTWL2", &mTWL2, &b_mTWL2);
   fChain->SetBranchAddress("mTW", &mTW, &b_mTW);
   fChain->SetBranchAddress("dPhiL1FJ1", &dPhiL1FJ1, &b_dPhiL1FJ1);
   fChain->SetBranchAddress("dPhiL2FJ1", &dPhiL2FJ1, &b_dPhiL2FJ1);
   fChain->SetBranchAddress("dEtaL1FJ1", &dEtaL1FJ1, &b_dEtaL1FJ1);
   fChain->SetBranchAddress("dEtaL2FJ1", &dEtaL2FJ1, &b_dEtaL2FJ1);
   fChain->SetBranchAddress("dRL1FJ1", &dRL1FJ1, &b_dRL1FJ1);
   fChain->SetBranchAddress("dRL2FJ1", &dRL2FJ1, &b_dRL2FJ1);

   fChain->SetBranchAddress("dPhiL1J1", &dPhiL1J1, &b_dPhiL1J1);
   fChain->SetBranchAddress("dPhiL2J1", &dPhiL2J1, &b_dPhiL2J1);
   fChain->SetBranchAddress("dEtaL1J1", &dEtaL1J1, &b_dEtaL1J1);
   fChain->SetBranchAddress("dEtaL2J1", &dEtaL2J1, &b_dEtaL2J1);
   fChain->SetBranchAddress("dRL1J1", &dRL1J1, &b_dRL1J1);
   fChain->SetBranchAddress("dRL2J1", &dRL2J1, &b_dRL2J1);

   fChain->SetBranchAddress("dPhiL1J2", &dPhiL1J2, &b_dPhiL1J2);
   fChain->SetBranchAddress("dPhiL2J2", &dPhiL2J2, &b_dPhiL2J2);
   fChain->SetBranchAddress("dEtaL1J2", &dEtaL1J2, &b_dEtaL1J2);
   fChain->SetBranchAddress("dEtaL2J2", &dEtaL2J2, &b_dEtaL2J2);
   fChain->SetBranchAddress("dRL1J2", &dRL1J2, &b_dRL1J2);
   fChain->SetBranchAddress("dRL2J2", &dRL2J2, &b_dRL2J2);

   fChain->SetBranchAddress("dPhiL1W", &dPhiL1W, &b_dPhiL1W);
   fChain->SetBranchAddress("dPhiL2W", &dPhiL2W, &b_dPhiL2W);
   fChain->SetBranchAddress("dEtaL1W", &dEtaL1W, &b_dEtaL1W);
   fChain->SetBranchAddress("dEtaL2W", &dEtaL2W, &b_dEtaL2W);
   fChain->SetBranchAddress("dRL1W", &dRL1W, &b_dRL1W);
   fChain->SetBranchAddress("dRL2W", &dRL2W, &b_dRL2W);

   fChain->SetBranchAddress("dPhiJ1J2", &dPhiJ1J2, &b_dPhiJ1J2);
   fChain->SetBranchAddress("dEtaJ1J2", &dEtaJ1J2, &b_dEtaJ1J2);
   fChain->SetBranchAddress("dRJ1J2", &dRJ1J2, &b_dRJ1J2);

   fChain->SetBranchAddress("Nel", &Nel, &b_Nel);
   fChain->SetBranchAddress("Lep1d0", &Lep1d0, &b_Lep1d0);
   fChain->SetBranchAddress("Lep2d0", &Lep2d0, &b_Lep2d0);
   fChain->SetBranchAddress("Lep3d0", &Lep3d0, &b_Lep3d0);
   fChain->SetBranchAddress("Lep1Eta", &Lep1Eta, &b_Lep1Eta);
   fChain->SetBranchAddress("Lep2Eta", &Lep2Eta, &b_Lep2Eta);
   fChain->SetBranchAddress("DRlllv", &DRlllv, &b_DRlllv);
   fChain->SetBranchAddress("DRlljj", &DRlljj, &b_DRlljj);
   fChain->SetBranchAddress("DRllJ", &DRllJ, &b_DRllJ);
   fChain->SetBranchAddress("DRlvjj", &DRlvjj, &b_DRlvjj);
fChain->SetBranchAddress("DRlvJ", &DRlvJ, &b_DRlvJ);
fChain->SetBranchAddress("mHHllJ", &mHHllJ, &b_mHHllJ);
fChain->SetBranchAddress("ptHHllJ", &ptHHllJ, &b_ptHHllJ);
fChain->SetBranchAddress("mHHlvJ", &mHHlvJ, &b_mHHlvJ);
fChain->SetBranchAddress("ptHHlvJ", &ptHHlvJ, &b_ptHHlvJ);
fChain->SetBranchAddress("mHHlljj", &mHHlljj, &b_mHHlljj);
fChain->SetBranchAddress("ptHHlljj", &ptHHlljj, &b_ptHHlljj);
fChain->SetBranchAddress("mHHlvjj", &mHHlvjj, &b_mHHlvjj);
fChain->SetBranchAddress("ptHHlvjj", &ptHHlvjj, &b_ptHHlvjj);
fChain->SetBranchAddress("mWlv", &mWlv, &b_mWlv);
fChain->SetBranchAddress("pTWlv", &pTWlv, &b_pTWlv);
fChain->SetBranchAddress("mZll", &mZll, &b_mZll);
fChain->SetBranchAddress("pTZll", &pTZll, &b_pTZll);
fChain->SetBranchAddress("Lep3isTight", &Lep3isTight, &b_Lep3isTight);
fChain->SetBranchAddress("Lep1isSignalL", &Lep1isSignalL, &b_Lep1isSignalL);
fChain->SetBranchAddress("Lep1isLooseL", &Lep1isLooseL, &b_Lep1isLooseL);
fChain->SetBranchAddress("Lep1isAntiBLL", &Lep1isAntiBLL, &b_Lep1isAntiBLL);
fChain->SetBranchAddress("Lep1isPLVTightL", &Lep1isPLVTightL, &b_Lep1isPLVTightL);
fChain->SetBranchAddress("Lep1isECIDSL", &Lep1isECIDSL, &b_Lep1isECIDSL);   
fChain->SetBranchAddress("Lep2isSignalL", &Lep2isSignalL, &b_Lep2isSignalL);
fChain->SetBranchAddress("Lep2isLooseL", &Lep2isLooseL, &b_Lep2isLooseL);
fChain->SetBranchAddress("Lep2isAntiBLL", &Lep2isAntiBLL, &b_Lep2isAntiBLL);
fChain->SetBranchAddress("Lep2isPLVTightL", &Lep2isPLVTightL, &b_Lep2isPLVTightL);
fChain->SetBranchAddress("Lep2isECIDSL", &Lep2isECIDSL, &b_Lep2isECIDSL);
fChain->SetBranchAddress("Lep3isSignalL", &Lep3isSignalL, &b_Lep3isSignalL);
fChain->SetBranchAddress("Lep3isLooseL", &Lep3isLooseL, &b_Lep3isLooseL);
fChain->SetBranchAddress("Lep3isAntiBLL", &Lep3isAntiBLL, &b_Lep3isAntiBLL);
fChain->SetBranchAddress("Lep3isPLVTightL", &Lep3isPLVTightL, &b_Lep3isPLVTightL);
fChain->SetBranchAddress("Lep3isECIDSL", &Lep3isECIDSL, &b_Lep3isECIDSL);
fChain->SetBranchAddress("fj1_isWJet50", &fj1_isWJet50, &b_fj1_isWJet50);
fChain->SetBranchAddress("fj1_isWJet80", &fj1_isWJet80, &b_fj1_isWJet80);
fChain->SetBranchAddress("fj1_isZJet50", &fj1_isZJet50, &b_fj1_isZJet50);
fChain->SetBranchAddress("fj1_isZJet80", &fj1_isZJet80, &b_fj1_isZJet80);
   fChain->SetBranchAddress("regionSS2l", &regionSS2l, &b_regionSS2l);
   fChain->SetBranchAddress("Njets", &Njets, &b_Njets);
   fChain->SetBranchAddress("pass3LRegion", &pass3LRegion, &b_pass3LRegion);
   fChain->SetBranchAddress("Reg3lMll", &Reg3lMll, &b_Reg3lMll);
   fChain->SetBranchAddress("Reg3lMhh", &Reg3lMhh, &b_Reg3lMhh);
   fChain->SetBranchAddress("Reg3lPthh", &Reg3lPthh, &b_Reg3lPthh);
   fChain->SetBranchAddress("region3l", &region3l, &b_region3l);

   fChain->SetBranchAddress("pass4Lepton", &pass4Lepton, &b_pass4Lepton);
   fChain->SetBranchAddress("Lep3Flav", &Lep3Flav, &b_Lep3Flav);
   fChain->SetBranchAddress("Lep3isLoose", &Lep3isLoose, &b_Lep3isLoose);
   fChain->SetBranchAddress("Lep3isNoBL", &Lep3isNoBL, &b_Lep3isNoBL);
   fChain->SetBranchAddress("passLeptonPt", &passLeptonPt, &b_passLeptonPt);
   fChain->SetBranchAddress("Mlll", &Mlll, &b_Mlll);
   fChain->SetBranchAddress("Lep1Q", &Lep1Q, &b_Lep1Q);
   fChain->SetBranchAddress("Lep2Q", &Lep2Q, &b_Lep2Q);

   fChain->SetBranchAddress("Mjj", &Mjj, &b_Mjj);
   fChain->SetBranchAddress("Ptjj", &Ptjj, &b_Ptjj);
   fChain->SetBranchAddress("fatjet1M", &fatjet1M, &b_fatjet1M);
   fChain->SetBranchAddress("passTrig", &passTrig, &b_passTrig);
   fChain->SetBranchAddress("passTrigMatch", &passTrigMatch, &b_passTrigMatch);
   fChain->SetBranchAddress("passSameSign", &passSameSign, &b_passSameSign);
   fChain->SetBranchAddress("pass3Lepton", &pass3Lepton, &b_pass3Lepton);
   fChain->SetBranchAddress("Nleptons", &Nleptons, &b_Nleptons);
   fChain->SetBranchAddress("Nfatjets", &Nfatjets, &b_Nfatjets);
   fChain->SetBranchAddress("dPhill", &dPhill, &b_dPhill);
   fChain->SetBranchAddress("Lep1Flav", &Lep1Flav, &b_Lep1Flav);
   fChain->SetBranchAddress("Lep2Flav", &Lep2Flav, &b_Lep2Flav);
   fChain->SetBranchAddress("mV3", &mV3, &b_mV3);
   fChain->SetBranchAddress("PtV3", &PtV3, &b_PtV3);
   fChain->SetBranchAddress("weight", &weight, &b_weight);
   fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
   fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
   fChain->SetBranchAddress("passGHHSS2lSR", &passGHHSS2lSR, &b_passGHHSS2lSR);
   fChain->SetBranchAddress("passSS2lSRreg1", &passSS2lSRreg1, &b_passSS2lSRreg1);
   fChain->SetBranchAddress("passSS2lSRreg2", &passSS2lSRreg2, &b_passSS2lSRreg2);
   fChain->SetBranchAddress("fatjet1Pt", &fatjet1Pt, &b_fatjet1Pt);
   fChain->SetBranchAddress("metSig", &metSig, &b_metSig);
   fChain->SetBranchAddress("met", &met, &b_met);
   fChain->SetBranchAddress("metSigRatio", &metSigRatio, &b_metSigRatio);
   fChain->SetBranchAddress("Mll", &Mll, &b_Mll);
   fChain->SetBranchAddress("Lep1Pt", &Lep1Pt, &b_Lep1Pt);
   fChain->SetBranchAddress("Lep2Pt", &Lep2Pt, &b_Lep2Pt);
   fChain->SetBranchAddress("Lep3Pt", &Lep3Pt, &b_Lep3Pt);
   fChain->SetBranchAddress("Jet1Pt",  &Jet1Pt,  &b_Jet1Pt);
   fChain->SetBranchAddress("Jet2Pt",  &Jet2Pt,  &b_Jet2Pt);
   fChain->SetBranchAddress("Jet1Eta",  &Jet1Eta,  &b_Jet1Eta);
   fChain->SetBranchAddress("Jet2Eta",  &Jet2Eta,  &b_Jet2Eta);
   fChain->SetBranchAddress("Nbjets",  &Nbjets,  &b_Nbjets);
   fChain->SetBranchAddress("Nbjets77",  &Nbjets77,  &b_Nbjets77);
   fChain->SetBranchAddress("Lep1isTight",  &Lep1isTight,  &b_Lep1isTight);  //no such variables available in the trees!...
   fChain->SetBranchAddress("Lep2isTight",  &Lep2isTight,  &b_Lep2isTight);
   fChain->SetBranchAddress("Lep1isLoose",  &Lep1isLoose,  &b_Lep1isLoose);
   fChain->SetBranchAddress("Lep1isNoBL",  &Lep1isNoBL,  &b_Lep1isNoBL);
   fChain->SetBranchAddress("Lep2isLoose",  &Lep2isLoose,  &b_Lep2isLoose);
   fChain->SetBranchAddress("Lep2isNoBL",    &Lep2isNoBL,    &b_Lep2isNoBL);
   fChain->SetBranchAddress("Lep1truthStatus", &Lep1truthStatus, &b_Lep1truthStatus);
   fChain->SetBranchAddress("Lep2truthStatus", &Lep2truthStatus, &b_Lep2truthStatus);
   fChain->SetBranchAddress("Lep3truthStatus", &Lep3truthStatus, &b_Lep3truthStatus);
   Notify();
}

Bool_t ReadTruthTree::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void ReadTruthTree::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t ReadTruthTree::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef ReadTruthTree_cxx
