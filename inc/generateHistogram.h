#ifndef generateHistogram_H
#define generateHistogram_H
#include <string>
#include <vector>
#include <map>

#include "TString.h"
#include "TH1.h"
#include "TH2.h"
#include "TChain.h"
#include "ReadTruthTree.h"
//#include "histSvc.h"
//#include "MVATree.h"
#include "TLorentzVector.h"
#include <iostream>
#include <fstream>
class MVATree;

class histSvc;

class generateHistogram {

public:

  // Samples
  enum AnalysisType {
    ZeroLepton,
    OneLepton,
    TwoLepton,
	ThreeLepton
  };

  enum AnalysisRegion {
    OneJet,
    TwoJetSR,
    ThreeJetSR,
    TwoJetCR,
    ThreeJetCR,
    ThreePJet
  };

  enum Process {
    nonprompt,
	GHH300X,
	GHH300Y,
	GHH600X,
	GHH600Y,
	GHH900X,
    ttbar,
    stop,
    Wjets,
    Zjets,
    WZ,
    ZZ,
	ssWW,
	WWW,
	VVV,
	Zgamma,
	Wgamma,
	ttZ,
	ttW,
	tZ,
	data, //Yanhui 
  };

  virtual void GenerateHistogram( AnalysisType type, TString DSID);

  virtual void FillHistograms(TString DSID);

  std::vector<AnalysisRegion> GetRegion( AnalysisType type );

  std::vector<unsigned int> compute_btagging( std::vector<TLorentzVector>& signalJets , std::vector<int>& signalJetFlavour, float& btagWeight );
  TString GetOutputPath( );

  void SetInputPath( TString path );
  void SetOutputPath( TString path );
  void SetNEvents( int events );
  void SetDebug( bool debug );
  void SetLumi( float lumi );
  void RunMJ (bool MJ);
  void RunCBA (bool CBA);
  void RunMC16a (bool MC16a);
  void UseSFs (bool UseSFs);
  void TightIso (bool TightIso);
  AnalysisType m_type;
  Process m_process;

  int m_nEvents = -1;
  TString m_DSID = "";
  int m_Debug = false;
  float m_Lumi = 1;
  float m_SumWeights = 0;
  float m_XSec = 0;
  bool m_MJ = false;
  bool m_CBA = false;
  bool m_MC16a = false;
  bool m_UseSFs = false;
  bool m_TightIso = false;
  TChain* m_inputChain;
  TString m_outputPath = "./";
  TString m_inputPath = "./";

  TH2D* hTruthMap_b = NULL;
  TH2D* hTruthMap_c = NULL;
  TH2D* hTruthMap_l = NULL;
  TH2D* hTruthMap_t = NULL;

  ReadTruthTree* readNtuple;
  MVATree *m_tree = NULL;
  histSvc *m_histFill = NULL;

  generateHistogram();
  ~generateHistogram();

private:

  void LoadTruthMaps();
  void FileLoader(TString DSID);
  int roll_dice(const std::vector<float>&choices);
  float getTagEff( TLorentzVector jet, int jetFlav );
  void StoreConfig( AnalysisType type, TString DSID );
  Process GetProcessFromDSID( TString DSID );

};

#endif
