#ifndef generateHistogram_2lep_H
#define generateHistogram_2lep_H

#include <string>
#include <vector>
#include <map>

#include "TString.h"
#include "TH1.h"
#include "TChain.h"
#include "ReadTruthTree.h"
#include "TLorentzVector.h"

#include "generateHistogram.h"

class generateHistogram_2lep : public generateHistogram {

public:

  void Selection2Lepton(TString DSID);

  virtual void FillHistograms(TString DSID) override;
//  virtual void GenerateHistogram( AnalysisType type, Process process, Variation variation ) override;

  float CalculateMtop(const TLorentzVector &lepton,
		      const TLorentzVector &MET,
		      const TLorentzVector &b_jet1,
		      const TLorentzVector &b_jet2);

  float CalculatedYWH(
    const TLorentzVector &lepton,
    const TLorentzVector &MET,
    const TLorentzVector &b_jet1,
    const TLorentzVector &b_jet2);
//  TChain* m_inputChain;
//  ReadTruthTree* readNtuple;

  generateHistogram_2lep();
  ~generateHistogram_2lep();


private:

};

#endif
